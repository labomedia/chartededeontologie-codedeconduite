# Une Charte de déontologie - un code de conduite ?

En lien avec le changement de genre de l'association La Labomedia, nous avons souhaité travailler à l'élaboration collective d'une charte de déontologie (ou code de conduite) qui permettrait de définir les comportements et pratiques que nous ne souhaitons pas rencontrer lorsque nous organisons des événements ou lors des activités régulières de l'association. Cela a pour objectif de favoriser l'inclusivité de activités de l'association, à la fois pour les femmes, queer, LGTBI, ainsi que pour les personnes issus d'horizons culturels et socio-économiques généralement peu présents dans ces lieux / contextes.

Par extension, cette charte peut également avoir pour vocation de concerner également notre rapport aux autres êtres vivants, aux choses inorganiques ainsi qu'aux formes naissantes d'intelligences artificielles.

Ressources et références à retrouver ici (Wiki de la Labomedia) : https://frama.link/charte-deontologie-LaLabomedia