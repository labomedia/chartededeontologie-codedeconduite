# Charte de deontologie - Code de conduite de La Labomedia

## WTF ?

Ne soyez pas grossier, ce document a justement pour vocation de favoriser le caractère inclusif, bienveillant et paisible des activités et contextes proposés par l'association La Labomedia.  
La Charte de déontologie tente d'établir les valeurs et les actions qui nous réunissent alors que le code de conduite constitue une liste non exhaustive de comportements que nous ne souhaitons pas rencontrer. Lorsque des manquements sont constatés, chacun est invité à y remédier en suivant le protocole proposé.

Chaque personne qui participe aux activités proposées se doit de lire cette Charte De Déontologie - Code De Conduite (CDDCDC) et d’y adhérer sans réserve, faute de quoi l’accès à ces activités pourra lui être refusé. Pour exprimer des réserves sur ce document ou y apporter des améliorations, postez un problème sur https://framagit.org/labomedia/chartededeontologie-codedeconduite/. Il est possible de discuter de cette CDDCDC et de sa mise en application avec un adhérent de l’association , un permanent, ou lors de l’Assemblée Générale, instance suprême qui l’a initialement accepté.

Le but est de faire en sorte que ce document reste lisible et engageant.

## J’aime
* Être accueillant vis à vis des êtres organiques, inorganiques et numériques de toutes origines et identités. Cela comprend, sans toutefois s' y limiter, les membres de toute espèce, ethnie, culture, origine nationale, couleur, statut d'immigration, classe sociale et économique, niveau de scolarité, identité de genre, orientation sexuelle, âge, taille, situation familiale, convictions politiques et philosophiques, religion, aptitudes mentales et physiques, degré d’autonomie vis à vis de la technologie
* Participer à un espace d’échange où chacun peut partager ses questionnements, ses connaissances sans crainte du ridicule, sans imposer son point de vue
* Utiliser un langage inclusif adapté à l’auditoire : les commentaires désobligeants ou plaisanteries déplacées, les menaces ou le langage violent ne sont pas acceptables, tout comme le fait de s’adresser aux autres participant·e·s d'une manière fâchée, intimidante ou humiliante
* Respecter les participant·e·s, leurs expériences et leurs différents points de vue
* Respecter les limites physiques et émotionnelles des personnes qui m’entourent, quelles que soient mes intentions. Cela implique de modifier mon comportement si une personne me signifie que je la mets mal à l'aise, ou de questionner la personne en cas de doute
* Respecter la vie privée, ne pas divulguer des informations personnelles concernant les participant·e·s sans leur consentement
* Ne pas enregistrer ni photographier tout élément reconnaissable sans l'accord des personnes concernées. Cela implique notamment les voix, tatouages, écrans d'ordinateurs, plans larges…
* Être attentif à la participation des autres, au juste partage de l’espace / temps de paroles lors de discussions et d’échanges en opérant à une écoute active et au respect de l’expression de chacun
* Respecter l’intégrité du matériel mis à ma disposition ou prêté par d’autres, la propreté des locaux de l’association, des lieux collectifs et de l’espace public, le bien être des voisins et des autres espèces qui cohabitent sur terre

## J’aime pas
* Être spectateur si je constate quelque chose d'inapproprié. Si je ne me sens pas à l'aise pour intervenir mais que je pense que quelqu'un devrait le faire, j’en avise une personne de confiance ou les personnes listés au paragraphe Comité D’Intervention Pour Une Diversité Paisible (CDIPUDP)

## Conclusion
Il faut travailler à la disparition de ce type de documents dont les éléments devraient être naturellement acquis lors de processus éducatifs, culturels et sociaux par l’ensemble de la population

# Composition du Comité D’Intervention Pour Une Diversité Paisible (CDIPUDP)
* En cas de manquement à l’un de ces points, si une réponse individuelle n’est pas apportée, le CDIPUDP pourra être sollicité pour définir une réponse approprié à la situation
* Il est à ce jour composé :
    * du président de l’association et des membres du Conseil d’Administration
    * des salariées permanents ou intermittents de l’association
    * d’adhérents volontaires désignés par l’Assemblé Générale 
* La réponse à un manquement avéré à cette CDDCDC pourra aller jusqu’à l’interdiction de participer aux activités de l’association et/ou à la saisie des autorités compétentes
* Pour le contacter : cdipudp@labomedia.org ou via le formulaire https://labomedia.org/contact/ (laisser une adresse mail éventuellement anonyme où vous pourrez lire la réponse)
